const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const include = [
  path.resolve(__dirname, 'assets')
];

module.exports = {
  entry: {
    content: './assets/content',
    background: './assets/background',
    popup: './assets/popup'
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].js',
    publicPath: '/'
  },
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        include,
        test: /\.(js|jsx)$/,
        use: {
          loader: 'babel-loader',
          options: {
            'presets': [
              'es2015',
              'stage-0'
            ]
          }
        }
      },
      {
        include,
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                importLoaders: 1
              }
            },
            {
              loader: 'less-loader',
              options: {
                sourceMap: true
              }
            }
          ]
        })
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: '[name].css',
      allChunks: true
    }),
    new CopyWebpackPlugin([
      { from: './assets/popup/popup.html' },
      { from: './manifest.json' },
      { from: './assets/icons', to: 'icons' }
    ]),
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'manifest'],
      minChunks: function(module) {
        return module.context && module.context.indexOf('node_modules') !== -1;
      }
    })
  ]
};
