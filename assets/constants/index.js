export const CURRENCIES = {
  USD: {
    name: 'US dollar',
    symbol: '\\$',
    abbr: ''
  },
  EUR: {
    name: 'Euro',
    symbol: '€',
    abbr: ''
  },
  JPY: {
    name: 'Japanese yen',
    symbol: '¥',
    abbr: ''
  },
  BGN: {
    name: 'Bulgarian lev',
    symbol: 'лв',
    abbr: ''
  },
  CZK: {
    name: 'Czech koruna',
    symbol: 'Kč',
    abbr: ''
  },
  DKK: {
    name: 'Danish krone',
    symbol: '',
    abbr: ''
  },
  GBP: {
    name: 'Pound sterling',
    symbol: '£',
    abbr: ''
  },
  HUF: {
    name: 'Hungarian forint',
    symbol: 'Ft',
    abbr: ''
  },
  PLN: {
    name: 'Polish zloty',
    symbol: 'zł',
    abbr: ''
  },
  RON: {
    name: 'Romanian leu',
    symbol: 'lei',
    abbr: ''
  },
  SEK: {
    name: 'Swedish krona',
    symbol: 'kr',
    abbr: ''
  },
  CHF: {
    name: 'Swiss franc',
    symbol: 'CHF',
    abbr: ''
  },
  NOK: {
    name: 'Norwegian krone',
    symbol: '',
    abbr: ''
  },
  HRK: {
    name: 'Croatian kuna',
    symbol: 'kn',
    abbr: ''
  },
  RUB: {
    name: 'Russian rouble',
    symbol: '₽',
    abbr: 'руб'
  },
  TRY: {
    name: 'Turkish lira',
    symbol: '',
    abbr: ''
  },
  AUD: {
    name: 'Australian dollar',
    symbol: '',
    abbr: ''
  },
  BRL: {
    name: 'Brazilian real',
    symbol: 'R\\$',
    abbr: ''
  },
  CAD: {
    name: 'Canadian dollar',
    symbol: '',
    abbr: ''
  },
  CNY: {
    name: 'Chinese yuan renminbi',
    symbol: '',
    abbr: 'RMB'
  },
  HKD: {
    name: 'Hong Kong dollar',
    symbol: '',
    abbr: ''
  },
  IDR: {
    name: 'Indonesian rupiah',
    symbol: 'Rp',
    abbr: ''
  },
  ILS: {
    name: 'Israeli shekel',
    symbol: '₪',
    abbr: ''
  },
  INR: {
    name: 'Indian rupee',
    symbol: '',
    abbr: ''
  },
  KRW: {
    name: 'South Korean won',
    symbol: '₩',
    abbr: ''
  },
  MXN: {
    name: 'Mexican peso',
    symbol: '',
    abbr: ''
  },
  MYR: {
    name: 'Malaysian ringgit',
    symbol: 'RM',
    abbr: ''
  },
  NZD: {
    name: 'New Zealand dollar',
    symbol: '',
    abbr: ''
  },
  PHP: {
    name: 'Philippine peso',
    symbol: '₱',
    abbr: ''
  },
  SGD: {
    name: 'Singapore dollar',
    symbol: '',
    abbr: ''
  },
  THB: {
    name: 'Thai baht',
    symbol: '฿',
    abbr: ''
  },
  ZAR: {
    name: 'South African rand',
    symbol: 'R',
    abbr: ''
  }
};

export const DEFAULT_CURRENCY_CODE = 'USD';

export const LOCAL_KEY = 'CC_CURRENCY';

export const TOOLTIP_ID = 'cc-converted-amount-tooltip';
