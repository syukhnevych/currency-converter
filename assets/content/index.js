import { CURRENCIES, TOOLTIP_ID } from '../constants';
import Tooltip from './Tooltip';
import styles from './content.less';

const tooltip = new Tooltip();
document.onmouseup = event => getTooltip(document.getSelection());

function getTooltip(selection) {
  if (!selection) {
    return;
  }

  getConvertedPrice(getPrice(selection.toString()), addTooltip.bind(selection));
}

function addTooltip(res) {
  tooltip.attach(this, res);

  setTimeout(() => tooltip.detach(), 500000);
}

function getPrice(selection) {
  const PRICE_PATTERN = /\d+([.,\ ]\d+)*/i;
  const CURRENCY_PATTERN = getCurrencyPattern();
  const price = {};

  let match;

  if (!PRICE_PATTERN.test(selection)) {
    console.log('Selection does not match the price pattern.');
    return;
  }

  match = selection.match(CURRENCY_PATTERN);

  if (!match) {
    console.log('Currency is not found in the selection please select text including the currency.');
    return;
  }

  price.currency = match[1];

  if (match[2]) {
    price.currency = getCurrencyCode('symbol', match[2]);
  }
  else if (match[3]) {
    price.currency = getCurrencyCode('abbr', match[3]);
  }

  price.amount = selection.replace(/[^0-9,.]/gi, '');

  if (!price.currency || !price.amount) {
    console.log('Currency or amount is undefined.');
    return;
  }

  return price;
}

function getConvertedPrice(price, callback) {
  if (!price) {
    return;
  }

  chrome.runtime.sendMessage(price, res => callback(res));
}

function getCurrencyPattern() {
  let currencyCodeRegex = '';
  let currencySymbolsRegex = '';
  let currencyAbbrRegex = '';

  for (const code in CURRENCIES) {
    if (!CURRENCIES.hasOwnProperty(code)) {
      continue;
    }

    const currency = CURRENCIES[code];

    if (currencyCodeRegex.length > 0) {
      currencyCodeRegex += '|';
    }

    currencyCodeRegex += `${code}`;

    if (currency.symbol) {
      if (currencySymbolsRegex.length > 0) {
        currencySymbolsRegex += '|';
      }

      currencySymbolsRegex += currency.symbol;
    }

    if (currency.abbr) {
      if (currencyAbbrRegex.length > 0) {
        currencyAbbrRegex += '|';
      }

      currencyAbbrRegex += currency.abbr;
    }
  }

  return new RegExp(`(${currencyCodeRegex})|(${currencySymbolsRegex})|(${currencyAbbrRegex})`, 'i');
}

function getCurrencyCode(key, value) {
  for (const code in CURRENCIES) {
    if (!CURRENCIES.hasOwnProperty(code)) {
      continue;
    }

    const currency = CURRENCIES[code];

    if (currency[key].toLowerCase() === value.toLowerCase()) {
      return code;
    }
  }
}
