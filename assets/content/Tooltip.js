import { TOOLTIP_ID } from '../constants';

export default class Tooltip {

  constructor(options) {
    this.settings = options;
    this._render();
  }

  set settings(options) {
    this.defaults = {};
    this._settings = Object.assign({}, this.defaults, options);
  }

  get settings() {
    return this._settings;
  }

  set coordinates(el) {
    const range = el.getRangeAt(0);
    const rect = range.getBoundingClientRect();

    this._coordinates = {
      x: rect.left + pageXOffset - rect.width / 2,
      y: rect.top + pageYOffset - rect.height
    }
  }

  get coordinates() {
    return this._coordinates;
  }

  _render() {
    this.el = document.createElement('div');
    this.el.id = TOOLTIP_ID;
    this.el.className = 'hidden';

    document.body.appendChild(this.el);
  }

  _toggleVisibility() {
    this.el.classList.toggle('hidden');
  }

  _setStyle() {
    this.el.style.left = this.coordinates.x + 'px';
    this.el.style.top = this.coordinates.y + 'px';
  }

  _unsetStyle() {
    this.el.style.left = null;
    this.el.style.top = null;
  }

  _setText(text) {
    this.el.innerText = text;
  }

  _unsetText() {
    this.el.innerText = null;
  }

  attach(el, text) {
    this.coordinates = el;

    this._setStyle();
    this._setText(text);
    this._toggleVisibility();
  }

  detach() {
    this._toggleVisibility();
    this._unsetStyle();
    this._unsetText();
  }
}
