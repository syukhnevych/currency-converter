import fx from 'money';
import axios from 'axios';
import { DEFAULT_CURRENCY_CODE, LOCAL_KEY } from '../constants';

chrome.runtime.onMessage.addListener(({ amount, currency }, sender, sendResponse) => {
  axios.get('https://api.fixer.io/latest')
      .then(({ data }) => {
        fx.base = data.base;
        fx.rates = data.rates;

        const from = currency.toUpperCase();
        const to = getSelectedCurrency();
        const rate = fx(amount).from(from).to(to);

        sendResponse(`${rate.toFixed(2)} ${to}`);
      });

  return true;
});

function getSelectedCurrency() {
  const currency = localStorage.getItem(LOCAL_KEY);

  return currency || DEFAULT_CURRENCY_CODE;
}
