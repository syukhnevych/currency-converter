import { CURRENCIES, LOCAL_KEY, DEFAULT_CURRENCY_CODE } from '../constants';

const options = [];
const checkbox = document.createElement('input');
const select = document.createElement('select');

checkbox.type = 'checkbox';
checkbox.checked = true;

for (const code in CURRENCIES) {
  if (!CURRENCIES.hasOwnProperty(code)) {
    continue;
  }

  const currency = CURRENCIES[code];
  const selectedCurrency = getSelectedCurrency();

  options.push(`<option value="${code}" ${code === selectedCurrency ? 'selected' : ''}>${currency.name}</option>`);
}

select.innerHTML = options.join('');

document.getElementById('root').appendChild(select);

select.onchange = event => localStorage.setItem(LOCAL_KEY, event.target.value);

function getSelectedCurrency() {
  const currency = localStorage.getItem(LOCAL_KEY);

  return currency || DEFAULT_CURRENCY_CODE;
}
